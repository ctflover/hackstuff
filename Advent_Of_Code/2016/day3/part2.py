count = 0

with open('input.txt', 'r') as file1:
    lines = file1.readlines()

columns = zip(*[map(int, line.split()) for line in lines])

for column in columns:
    for i in range(0, len(column), 3):
        sides = column[i:i+3]

        if sides[0] + sides[1] > sides[2] and sides[1] + sides[2] > sides[0] and sides[0] + sides[2] > sides[1]:
            count += 1

print(count)

