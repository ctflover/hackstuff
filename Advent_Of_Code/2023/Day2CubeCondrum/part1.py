import sys
from collections import defaultdict

target = defaultdict(int, {"red": 12, "blue": 14, "green": 13})

with open(sys.argv[1], 'r') as f:
    lines = [l.strip() for l in f.readlines()]

part1 = 0
for line in lines:
    game_str, pull_str = line.split(': ')
    game_id = int(game_str.split(' ')[-1])
    game_valid = True
    for pull in pull_str.split('; '):
        for color_str in pull.split(', '):
            try:
                num_str, color = color_str.split(' ')
                num = int(num_str)
                if target[color] < num:
                    game_valid = False
            except ValueError:
                print(f"Error parsing line: {line}")
                print(f"Problematic color string: {color_str}")
                continue

    if game_valid:
        part1 += game_id

print(part1)

