numbers_dict = {
    "one": "one1one",
    "two": "two2two",
    "three": "three3three",
    "four": "four4four",
    "five": "five5five",
    "six": "six6six",
    "seven": "seven7seven",
    "eight": "eight8eight",
    "nine": "nine9nine"
}

file_path = "input"  

with open(file_path, 'r') as file:
    content = file.read()

for word, replacement in numbers_dict.items():
    content = content.replace(word, replacement)

with open('output', 'w') as file:
    file.write(content)

def MakeLists():
  c=""
  lists=[]
  f= open("output","r")
  lines=f.readlines()
  for line in lines:
    for i in line:
      if i.isdigit():
        c=c+i
        #b+=int(i)
    lists.append(c)
    c=""
    
  return(lists)

def GetSum():
  add=0
  lists = MakeLists()
  for i in lists:
    character=i[0]+i[-1]
    add=int(character)+add
  print(add)
GetSum()
