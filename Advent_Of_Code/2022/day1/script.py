def partone():
  calories = 0
  all_calories = []
  with open('input','r') as f:
    for line in f.readlines():
      
      for i in line.split():
        calories = calories+int(i)
      if not line.strip():
        all_calories.append(calories)
        calories=0
  print(max(all_calories))


def parttwo():
  calories = 0
  all_calories = []
  with open('input','r') as f:
    for line in f.readlines():
      
      for i in line.split():
        calories = calories+int(i)
      if not line.strip():
        all_calories.append(calories)
        calories=0
  output = (sorted(all_calories))
  print(sum(output[-3:]))

partone()
parttwo()
