import sys
c=0
d=0
with open(sys.argv[1], 'r') as f:
  lines = f.readlines()

part1 =0
for line in lines:
  list1=[]
  list2=[]
  e1,e2 = line.split(',')
  x1 = list(map(int, e1.split('-')))
  x2 = list(map(int, e2.split('-')))
  #print(x1)
  #print(x2)
  #print(x2[0])
  for i in range(x1[0],(x1[1])+1):
    #print(i)
    list1.append(i)
  for i in range(x2[0],(x2[1])+1):
    #print(i)
    list2.append(i)
  #print(list2)
  all_in_list1 = all(elem in list1 for elem in list2)
  all_in_list2 = all(elem in list2 for elem in list1)
  if(all_in_list1 or all_in_list2):
    c+=1

  intersection = set(list1).intersection(set(list2))
  if(intersection):
    d+=1


print("Part 1:",c)
print("Part 2:",d)
list1=[]
list2=[]
