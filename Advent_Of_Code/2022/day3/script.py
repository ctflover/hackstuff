def partone():
    priority = 0
    with open("input", "r") as f:

        for l in f.readlines():
            a = set(l[int(len(l) / 2) :])
            b = set(l[: int(len(l) / 2)])

            common_items = list(a.intersection(b))

            for i in common_items:
                if i.islower():
                    priority += ord(i) - 96
                if i.isupper():
                    priority += ord(i) - 64 + 26

    print(f"Part1: {priority}")


def parttwo():
    with open("input", "r") as f:
        line = f.readlines()
    priority = 0
    i = 0
    while i < len(line):
        for c in line[i]:
            if c in line[i] and c in line[i + 1] and c in line[i + 2]:
                if c.islower():
                    priority += ord(c) - 96
                if c.isupper():
                    priority += ord(c) - 64 + 26
                break

        i += 3
    print(f"Part2: {priority}")


partone()
parttwo()
