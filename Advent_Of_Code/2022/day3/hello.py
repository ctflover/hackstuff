priority = 0
with open('input', 'r') as f:
    for l in f.readlines():
        a = set(l[int(len(l)/2):])
        b = set(l[:int(len(l)/2)])
        common_items = list(a.intersection(b))
        print(common_items)
        
        for i in common_items:
            if i.islower():
                priority += ord(i) - ord('a') + 1
            elif i.isupper():
                priority += ord(i) - ord('A') + 27

print(f"Part1: {priority}")

