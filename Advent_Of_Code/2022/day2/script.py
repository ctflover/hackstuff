def part1():
  total_score = 0
  with open('input','r') as f:
    for line in f.readlines():
      a= (line.split())
      if a[0] == 'A':
        if(ord(a[0])-ord(a[1]) == -24):
          total_score+=6+(ord(a[1])-ord('W'))
        if(ord(a[0])-ord(a[1]) == -23):
          total_score+=3+(ord(a[1])-ord('W'))
        if(ord(a[0])-ord(a[1]) == -25):
          total_score+=0+(ord(a[1])-ord('W'))
          

      if a[0] == 'B':
        if(ord(a[0])-ord(a[1]) == -24):
          total_score+=6+(ord(a[1])-ord('W'))
        if(ord(a[0])-ord(a[1]) == -23):
          total_score+=3+(ord(a[1])-ord('W'))
        if(ord(a[0])-ord(a[1]) == -22):
          total_score+=0+(ord(a[1])-ord('W'))


      if a[0] == 'C':
        if(ord(a[0])-ord(a[1]) == -21):
          total_score+=6+(ord(a[1])-ord('W'))
        if(ord(a[0])-ord(a[1]) == -23):
          total_score+=3+(ord(a[1])-ord('W'))
        if(ord(a[0])-ord(a[1]) == -22):
          total_score+=0+(ord(a[1])-ord('W'))
          
  print(total_score)
  
#part1()
def part2():
  scores = {
  "A X": 3+0,
  "A Y": 1 + 3,
  "A Z": 2+6,
  "B X": 1+0,
  "B Y": 2 + 3,
  "B Z": 3+6,
  "C X": 2+0,
  "C Y": 3 + 3,
  "C Z": 1+6,
  }
  with open('input', 'r') as f:
    lines = f.readlines()

  part2 = sum([scores[l.strip()] for l in lines])
  print(part2)

part1()
part2()



