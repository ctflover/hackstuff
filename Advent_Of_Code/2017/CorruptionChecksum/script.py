def part1():
    c=0
    output = 0
    with open('input.txt','r') as f:
        while c!=16:
            data = f.readline()
            data_list = data.split()
            evaluated_list = []
            for item in data_list:
                evaluated_item = eval(item)
                evaluated_list.append(evaluated_item)
            # print(evaluated_list)
            # output = ((max(evaluated_list) - min(evaluated_list)))
            # print(max(evaluated_list) - min(evaluated_list))
            output+= (max(evaluated_list)-min(evaluated_list))

            c=c+1
        
    # print(res)
    print(output)

    
def part2():
    c=0
    output = 0
    with open('input.txt','r') as f:
        while c!=16:
            data = f.readline()
            data_list = data.split()
            evaluated_list = []
            for item in data_list:
                evaluated_item = eval(item)
                evaluated_list.append(evaluated_item)
            # print(evaluated_list)
            # output = ((max(evaluated_list) - min(evaluated_list)))
            for i in evaluated_list:
                for j in evaluated_list:
                    if i%j == 0 and i!=j:
                        output+=i/j

            c=c+1
        
    # print(res)
    print(int(output))

part1()
part2()