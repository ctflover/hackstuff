import re
import signal
import subprocess
import argparse
import requests
import os
import json

d=0

def map_hashes(file): # This function groups different types of hashes together.
    c=0

    print("[+] Spliting hashes to different files.")
    with open(file,'r') as f:
        lines = f.readlines()
        length = [line.strip() for line in lines if line.strip()]
        # # print(f'Total number of lines in the file: {len(lines)}')
        length_lines = len(lines)

        for line in lines:
            url = f"https://hashes.com/en/api/identifier?hash={line}" # API to identify hash type.
            r = requests.get(url)
            hash_type = json.loads(r.text)
            try:
                algorithms_list = hash_type["algorithms"][0]
                hash_algorithm = ''.join(algorithms_list).upper() # Stores the type of hash 
                hash_algorithm = hash_algorithm.replace('$PLAINTEXT', '$pass')
                hash_algorithm = hash_algorithm.replace('$SALT', '$salt')
                # print(hash_algorithm)
            except:
                # print(line.strip('\n'))
                l = line
                line = ':'.join(line.strip('\n').split(":")[1:])
                url = f"https://hashes.com/en/api/identifier?hash={line}" # API to identify hash type.
                r = requests.get(url)
                hash_type = json.loads(r.text)
                try:
                    algorithms_list = hash_type["algorithms"][0]
                    hash_algorithm = ''.join(algorithms_list).upper() # Stores the type of hash 
                    hash_algorithm = hash_algorithm.replace('$PLAINTEXT', '$pass')
                    hash_algorithm = hash_algorithm.replace('$SALT', '$salt')
                    # print(hash_algorithm)
                    with open(f"hash_crack/hash_types/{hash_algorithm}-user",'a') as g:
                        g.write(l)
                        continue
                except:
                    if line.strip():
                        # TODO - Ask the user If he's aware of the unkown hash and specify a mode or hash_algo.
                        print(line.strip('\n') + "- unknown")
                        c+=1
                        if(length_lines == c):
                            print("No hashes in the file are valid, try specifying the hash mode with -m if you are sure the hashes are correct.")
                            exit()
                    continue

            if not os.path.exists("hash_crack"):
                os.makedirs("hash_crack")

            if not os.path.exists("hash_crack/hash_types"):
                os.makedirs("hash_crack/hash_types")

            hash_algorithm = hash_algorithm.replace("/","_") 
            with open(f"hash_crack/hash_types/{hash_algorithm}",'a') as g:
                line = ' '.join(line.split())
                g.write(line+"\n") # Writes different types of hashes into different files.
    remove_duplicates_from_directory("hash_crack/hash_types")

def remove_duplicates_from_file(file_path):
    # Read lines from the file
    with open(file_path, 'r') as f:
        lines = f.readlines()

    # Remove duplicate lines
    unique_lines = set(lines)

    # Write unique lines back to the file
    with open(file_path, 'w') as f:
        f.writelines(unique_lines)

def remove_duplicates_from_directory(directory_path):
    print("[+] Removing duplicates line if present")
    # Iterate over all files in the directory
    for filename in os.listdir(directory_path):
        file_path = os.path.join(directory_path, filename)
        
        # Check if the path is a file
        if os.path.isfile(file_path):
            remove_duplicates_from_file(file_path)


def hash_mode(file): # This function returns hash mode.
    file = ''.join(file.split("/")[-1:])
    file = file.split('-')
    file[0] = file[0].replace("_","/")
    # url = "https://hashcat.net/wiki/doku.php?id=example_hashes" # TODO Rather than making request to site every time make a local copy of the source
    # r = requests.get(url)
    # print(r.text)
    with open("hashcat_example_hashes",'r') as h:
        # h.read()
        pattern = re.compile(f'> (.*?) </td><td class="col1"> {file[0]} </td>', re.IGNORECASE)
        matches = pattern.finditer(h.read())
        for match in matches:
            
            return(match.group(1))


    def signal_handler(signum, frame): # Function to get an input from user if the tool is not able to find any hash_mode using Regular Expressions.
        raise TimeoutError

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(30)  # 1/2 minute timeout

    try:
        hash_mode = input(f"No matches found for {file}. Please enter the hash mode within 30 seconds: ")
        signal.alarm(0)  # Disable the alarm
        return hash_mode
    except TimeoutError:
        print("Timeout: unable to get hash mode from user within 1/2 minute.")
        return None  # or some default value

#  hash_mode("hash_crack/hash_types/sha256\(\$pass\.\$salt\)")

def escape_filename(filename): # TODO There is some problem with regex as it is not able to identify hash mode of larger files as its not able to escape the characters correctly I guess.
    global d
    if d == 0:
        print("[+] Escaping Special Characters.")
        d += 1
    # List of characters to escape
    special_chars = ['$','(',')','.','*',' ','&','#','!','^','@']

    for char in special_chars:
        filename = filename.replace(char, f'\\{char}')

    return filename

def list_files(directory):
    files = []
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        if os.path.isfile(filepath):
            escaped_filename = escape_filename(filename)
            files.append(escaped_filename)
    return files

#map_hashes()
def main():
    parser = argparse.ArgumentParser(description="Execute hash_crack_automater.py with provided parameters.")
    parser.add_argument('-f', '--file', required=True, help="Hash File")
    parser.add_argument('-w','--wordlist', type=str, required=True, help='Wordlist file')
    parser.add_argument('-r', '--rules', required=False, help="hashcat rules file optional")
    args = parser.parse_args()

    map_hashes(args.file)
    
    directory_path = "hash_crack/hash_types"  
    files = list_files(directory_path)

    for file in files:
        mode = hash_mode(f"hash_crack/hash_types/{file}")
        if mode is None:
            print(f"Tool was not able to find hash_mode for {file}")
        else:
            hashcat_cmd = f"hashcat hash_crack/hash_types/{file} -m {mode} "
            
            if args.wordlist and args.wordlist != "/usr/share/wordlists/rockyou.txt":
                hashcat_cmd = f"hashcat -m {mode} hash_crack/hash_types/{file} {args.wordlist} "

            if args.wordlist and args.wordlist == "/usr/share/wordlists/rockyou.txt":
                hashcat_cmd = f"hashcat -m {mode} hash_crack/hash_types/{file} {args.wordlist} "
            
            if args.rules:
                hashcat_cmd = f"hashcat -m {mode} hash_crack/hash_types/{file} {args.wordlist} -r {args.rules}"
            
            if "-user" in file:
                hashcat_cmd += " --user"
            print(hashcat_cmd)
            crack_hash(hashcat_cmd, file, mode)

def crack_hash(hashcat_cmd,file,mode):
    print(f"[+] Executing Command {hashcat_cmd} You can press ctrl+c to jump to the next hash")
    try:
        result = subprocess.run(hashcat_cmd, shell=True, capture_output=True, text=True)
    except KeyboardInterrupt:
        print("[+] Hash cracking interrupted by user.")
        return None
    recovered_match = re.search(r'Recovered\.\.\.\.\.\.\.\.: (\d+)/(\d+)', result.stdout)

    if recovered_match:
        recoverd_hash = int(recovered_match.group(1))
        total_hash = int(recovered_match.group(2))

        if recoverd_hash > 0:
            try:
                hashcat_show_cmd = f"hashcat -m {mode} hash_crack/hash_types/{file} --show"
                show_result = subprocess.run(hashcat_show_cmd, shell=True, capture_output=True, text=True, check=True)
            except subprocess.CalledProcessError:
                hashcat_show_cmd = f"hashcat -m {mode} hash_crack/hash_types/{file} --show --user"
                show_result = subprocess.run(hashcat_show_cmd, shell=True, capture_output=True, text=True)
                
            cracked_data = show_result.stdout.split('\n')
            with open('hash_crack/cracked.txt', 'a') as f:
                for line in cracked_data:
                    if line:
                        f.write(line + '\n')

if __name__ == "__main__":
    main()
    file_path = "hash_crack/cracked.txt"
    if os.path.exists(file_path):
        print("[+] Cracked hashes:")
        with open(file_path, "r") as file:
            print(file.read())
