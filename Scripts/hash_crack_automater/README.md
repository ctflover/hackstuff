Usage: python3 hash_crack_automater.py -f [Input_File]

Optional Parameters: -w [wordlist file], -r [Rules Files]

 With this tool, you can feed it a mix of hashes from different hashing algorithms, and it intelligently identifies the algorithms using an API. It then organizes the hashes into separate files and applies regex magic to determine the appropriate hashcat mode for each. It then runs hashcat on all the files and print the cracked hashes.

Example:

```python
python3 hash_crack_automater.py -f hash -w /usr/share/wordlists/rockyou.txt 
[+] Spliting hashes to different files.
[+] Removing duplicates line if present
[+] Escaping Special Characters.
hashcat -m 1000 hash_crack/hash_types/NTLM /usr/share/wordlists/rockyou.txt 
[+] Executing Command hashcat -m 1000 hash_crack/hash_types/NTLM /usr/share/wordlists/rockyou.txt  You can press ctrl+c to jump to the next hash
hashcat -m 0 hash_crack/hash_types/MD5-user /usr/share/wordlists/rockyou.txt  --user
[+] Executing Command hashcat -m 0 hash_crack/hash_types/MD5-user /usr/share/wordlists/rockyou.txt  --user You can press ctrl+c to jump to the next hash
hashcat -m 100 hash_crack/hash_types/SHA1 /usr/share/wordlists/rockyou.txt 
[+] Executing Command hashcat -m 100 hash_crack/hash_types/SHA1 /usr/share/wordlists/rockyou.txt  You can press ctrl+c to jump to the next hash
hashcat -m 3200 hash_crack/hash_types/BCRYPT\ \$2\*\$,\ BLOWFISH\ \(UNIX\) /usr/share/wordlists/rockyou.txt 
[+] Executing Command hashcat -m 3200 hash_crack/hash_types/BCRYPT\ \$2\*\$,\ BLOWFISH\ \(UNIX\) /usr/share/wordlists/rockyou.txt  You can press ctrl+c to jump to the next hash
[+] Cracked hashes:
1d92601969eb78c24b5cc6321c440d8b:francis
test:82080600934821faf0bc59cba79964bc:P@ssword123
cbfdac6008f9cab4083784cbd1874f76618d2a97:password123
$2y$10$HQ2tYYn3mVzkwE.tajUMxeACJhlQBJfmfAHa16Tvs.ua/qRRg6jdK:123456789
```
